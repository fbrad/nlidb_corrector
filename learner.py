import logging
import random
import torch
import os
from torch import nn, optim
from utils import make_batches

class Learner:
	def __init__(self, model, train_data, dev_data, current_epoch = 0):
		self.model = model
		self.args = model.args
		self.train_data = train_data
		self.dev_data = dev_data
		self.current_epoch = current_epoch
		self.indices = list(range(len(train_data)))
		self.eval_indices = list(range(len(dev_data)))
		self.num_classes = 4
		self.criterion = nn.CrossEntropyLoss()
		if self.args.optimizer == 'adam':
			self.optimizer = optim.Adam(self.model.parameters(), lr = self.args.lr)
		elif self.args.optimizer == 'sgd':
			self.optimizer = optim.SGD(self.model.parameters(), lr = self.args.lr)

	def load_model(self, model):
		self.model = model

	def train(self):
		batches = make_batches(len(self.train_data), self.args.batch_size)
		for epoch in range(self.current_epoch, self.current_epoch + self.args.max_epoch):
			logging.info("epoch number %d" % epoch)

			#shuffle examples
			random.shuffle(self.indices)

			epoch_loss = 0.0
			for batch_num, (batch_idx_start, batch_idx_end) in enumerate(batches):
				self.model.zero_grad()

				input = self.train_data.get_network_inputs(self.indices[batch_idx_start:batch_idx_end],\
														self.args.device)
				labels = input[-1]

				#B x 4
				output = self.model(input)
				batch_size = output.size(0)

				batch_loss = self.criterion(output, labels)

				if batch_num % 20 == 0:
					logging.info("batch loss, epoch %d, %d/%d = %f" % (epoch, batch_idx_start,
																len(self.train_data), batch_loss))
					if self.args.debug:
						for named_param, param in self.model.named_parameters():
							logging.debug("%s size %f" % (named_param, param.norm()))

				batch_loss.backward()

				torch.nn.utils.clip_grad_norm_(list(self.model.parameters()), self.args.max_grad_norm)

				self.optimizer.step()

				epoch_loss += batch_loss.item() * batch_size

			epoch_loss /= len(self.train_data)
			logging.info("cumulative loss, epoch %d = %f" % (epoch, epoch_loss))

			#checkpoint model
			checkpoint = {
				'model' : self.model.state_dict(),
				'args' : self.args,
				'opt' : self.optimizer,
				'epoch' : epoch,
				'loss' : epoch_loss
			}

			torch.save(checkpoint, os.path.join(self.args.output_dir, 'model_epoch%d.pt' % epoch))

	def eval(self, epoch = 0):
		logging.debug("Evaluating model at epoch %d" % epoch)
		batches = make_batches(len(self.dev_data), self.args.batch_size)
		num_correct, num_examples = 0, len(self.dev_data)
		epoch_loss = 0.0
		#conf[i,j] = how many examples predicted as class i with ground truth class j
		confusion_matrix = torch.zeros(self.num_classes, self.num_classes)
		with torch.no_grad(): #disable gradient computation
			for (batch_idx_start, batch_idx_end) in batches:
				batch_size = batch_idx_end - batch_idx_start + 1

				#prepare input
				input = self.dev_data.get_network_inputs(self.eval_indices[batch_idx_start:batch_idx_end],
														 self.args.device)
				labels = input[-1]

				#compute probabilities and loss
				output = self.model(input)
				batch_loss = self.criterion(output, labels)

				#compute loss/accuracy
				epoch_loss += batch_loss.item() * batch_size
				predictions = torch.argmax(output, 1)
				logging.debug("batch_loss @idx %d = %f" % (batch_idx_start, batch_loss.item()))
				logging.debug("predictions = %s" % predictions)

				#compute confusion matrix/accuracy
				for (y_pred, y_correct) in zip(predictions, labels):
					confusion_matrix[y_pred, y_correct] += 1
				num_correct_batch = torch.sum(predictions == labels).item()
				num_correct += num_correct_batch

		accuracy = num_correct / num_examples
		epoch_loss /= num_examples
		logging.info("epoch %d stats: loss = %f, acc = %f" % (epoch, epoch_loss, accuracy))

		return epoch_loss, accuracy, confusion_matrix

	# test if network overfits first example
	def debug_overfit(self):
		batches = make_batches(len(self.train_data), self.args.batch_size)
		debug_indices = len(self.train_data) * [0]

		for batch_num, (batch_idx_start, batch_idx_end) in enumerate(batches):
			self.model.zero_grad()

			#prepare input
			input = self.train_data.get_network_inputs(debug_indices[batch_idx_start:batch_idx_end], \
													   self.args.device)
			labels = input[-1]

			#comppute loss
			output = self.model(input) #B x 4
			batch_size = output.size(0)
			batch_loss = self.criterion(output, labels)
			logging.info("loss %d/%d = %f" % (batch_idx_start, len(self.train_data), batch_loss))
			#logging.info("	output probs = %s, labels = %s" % (output, labels[0]))

			#compute gradient and update weights
			batch_loss.backward()
			torch.nn.utils.clip_grad_norm_(list(self.model.parameters()), self.args.max_grad_norm)
			self.optimizer.step()

















