import torch
import argparse
import os
import logging
from utils import init_logging, deserialize_from_file
from preproc import create_embedding_layer
from model import Model
from learner import Learner

parser = argparse.ArgumentParser()
parser.add_argument('-data', default='data', help='path to training, eval and vocabulary data')
parser.add_argument('-output_dir', default='output')
parser.add_argument('-model', default=None, help='path to existing model')

parser.add_argument('-word_embed_dim', default=600, type=int)
parser.add_argument('-freeze_emb', dest='freeze_emb', action='store_true')
parser.add_argument('-no_freeze_emb', dest='freeze_emb', action='store_false')
parser.set_defaults(freeze_emb=True)

parser.add_argument('-sql_embed_dim', default=256, type=int)
parser.add_argument('-word_encoder_hidden_dim', default=512, type=int)
parser.add_argument('-sql_encoder_hidden_dim', default=512, type=int)
parser.add_argument('-encoder_hidden_dim', default=512, type=int)
parser.add_argument('-dropout_rate', default=0.0, type=float)

parser.add_argument('-optimizer', default='adam')
parser.add_argument('-lr', default=0.1, type=float)
parser.add_argument('-max_epoch', default=50, type=int)
parser.add_argument('-batch_size', default=128, type=int)
parser.add_argument('-max_grad_norm', default=5.0, type=float)
parser.add_argument('-gpu', type=int, default=0)

parser.add_argument('-debug', default=False, type=bool, help='if true, run with debug tools on')

if __name__ == '__main__':
	args = parser.parse_args()

	# setup logging
	init_logging(os.path.join(args.output_dir, 'eval.log'),
				 logging.DEBUG if args.debug else logging.INFO)

	logging.info("Argument set: %s" % args)

	# setup device
	if args.gpu >= 0 and torch.cuda.is_available():
		args.device = torch.device('{}:{}'.format('cuda', args.gpu))
	else:
		args.device = torch.device('cpu')
	logging.info("Setup device %s " % args.device)

	# prepare data
	dev_data = deserialize_from_file(os.path.join(args.data, "dev.bin"))
	vocabs = [dev_data.annot_vocab, dev_data.sql_vocab]
	embedding_layers = [create_embedding_layer(vocab, False) for vocab in vocabs]

	# evaluate models
	losses, accs = [], []
	for idx in range(0, 80):
		model_name = "model_epoch%d.pt" % idx

		# load existing checkpoint
		model_path = os.path.join(args.output_dir, model_name)
		logging.info("Loading existing model at %s" % model_path)

		checkpoint = torch.load(model_path, map_location=args.device)

		model = Model(checkpoint['args'], embedding_layers).to(device=args.device)
		model.load_state_dict(checkpoint['model'])  # should overwrite Embedding weights as well
		epoch = checkpoint['epoch']
		assert epoch == idx, "Wrong epoch assigned to model"

		learner = Learner(model, [], dev_data, epoch)
		loss, acc, conf = learner.eval(epoch)
		losses.append(loss)
		accs.append(acc)

		precision = []
		for class_idx in range(4):
			precision.append((conf[class_idx, class_idx] / conf[class_idx].sum()).item())
			logging.info("  precision class %d: %f" % (class_idx, precision[-1]))

