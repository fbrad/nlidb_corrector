import logging
from collections import defaultdict, OrderedDict
import pickle as cPickle
import torch
import numpy
import codecs
import os
import re
import json

def init_logging(file_name, level=logging.INFO):
    formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(module)s: %(message)s', datefmt='%m/%d/%Y %H:%M:%S')
    fh = logging.FileHandler(file_name)
    ch = logging.StreamHandler()

    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    logging.getLogger().handlers = []
    logging.getLogger().addHandler(ch)
    logging.getLogger().addHandler(fh)
    logging.getLogger().setLevel(level)

    logging.info('init logging file [%s]' % file_name)

#Creates a boolean mask from sequence lengths.
def sequence_mask(lengths, max_len=None):
    batch_size = lengths.numel()
    max_len = max_len or lengths.max()
    return (torch.arange(0, max_len).type_as(lengths).repeat(batch_size, 1).lt(lengths.unsqueeze(1)))

#size: dataset size
#batch_size: size of batch
#Return array of (batch_start_index, batch_end_index) tuples
def make_batches(size, batch_size):
    nb_batch = int(numpy.ceil(size/float(batch_size)))
    return [(i*batch_size, min(size, (i+1)*batch_size)) for i in range(0, nb_batch)]


def serialize_to_file(obj, path, protocol=cPickle.HIGHEST_PROTOCOL):
    f = open(path, 'wb')
    cPickle.dump(obj, f, protocol=protocol)
    f.close()

def deserialize_from_file(path):
    f = open(path, 'rb')
    obj = cPickle.load(f)
    f.close()
    return obj

def extract_ngram_dict(glove_dict, n):
    ngram_dict_list = []
    for l in range(0, n):
        ngram_dict = {k: glove_dict[k] for k in glove_dict.keys() if len(k) == (l + 1)}
        ngram_dict_list.append(ngram_dict)

    return ngram_dict_list

def get_avg_token_embedding(word, glove_dict, embed_size=300):
    sum_word_emb = torch.zeros(embed_size)
    num_toks = 0
    pattern = '([\[\](){}*@&=#,.:;%$\\\'`^"_\-–²+|<>?!/\s])'
    filter_toks = {'\xa0', '', ' '}
    col_toks = list(filter(lambda tok: tok not in filter_toks, re.split(pattern, word)))
    #print("[get_avg_token_embedding] |", word, "|:", col_toks)
    # average token embedding
    for col_tok in col_toks:
        if col_tok in glove_dict:
            sum_word_emb += glove_dict[col_tok]
            num_toks += 1
        else:
            pass
            # print("unknown: ", col_tok)
            # print("original: ", word)

    if num_toks > 0:
        sum_word_emb = sum_word_emb / num_toks

    return sum_word_emb

#create dictionary with character unigrams, bigrams, trigrams from Glove dataset
def get_ngram_embedding(ngram, ngram_dict_list, embed_size):
    for ngram_dict in ngram_dict_list:
        if ngram in ngram_dict:
            return ngram_dict[ngram]

    return torch.zeros(embed_size)


# return average of ngram character embeddings contained in word
def get_avg_ngram_embedding(word, n, ngram_dict_list, embed_size=300):
    # take unigrams, bigrams,...
    l = len(word)
    ngrams = []
    for ngram_len in range(1, n + 1):
        for i in range(l - ngram_len + 1):
            ngrams.append(word[i:i + ngram_len])

    # compute avg embedding
    s = torch.zeros(embed_size)
    valid_ngrams = 0
    for ngram in ngrams:
        emb = get_ngram_embedding(ngram, ngram_dict_list, embed_size)
        s += emb
        if torch.sum(emb) != 0:
            valid_ngrams += 1
        else:
            pass
            # print("unknown ", ngram)

    if valid_ngrams > 0:
        return s / valid_ngrams
    else:
        return s

# Generates the column vocabulary for the WikiSQL dataset.
def generate_wiki_column_vocab(wikisql_path, glove_path, embed_type=False):
    # retrieve/generate table columns
    print("[generate_wiki_column_vocab] create headers dictionary")
    headers_path = os.path.join(wikisql_path, "headers.json")
    if os.path.exists(headers_path):
        headers_dict = json.load(open(headers_path, "r"))
    else:
        print("No headers found, abort")
        return

    # retrieve Glove embeddings
    print("[generate_wiki_column_vocab] retrieve Glove embeddings")
    glove_dict = deserialize_from_file(os.path.join(glove_path, "glove_embeddings.bin"))

    # generate dictionary with character ngrams of lengths 1-4
    char_ngram_dict_list = extract_ngram_dict(glove_dict, 4)

    # compute embeddings for columns
    print("[generate_wiki_column_vocab] create column embeddings")
    column_dict = {}
    for column_info in headers_dict["train"].values():
        column_types = column_info["types"]
        column_header = [s.lower() for s in column_info["header"]]

        for col_type, col_name in zip(column_types, column_header):
            if col_name in column_dict:
                continue
            if embed_type:
                col_type_name = col_name + " " + col_type
            else:
                col_type_name = col_name

            word_embed = get_avg_token_embedding(col_type_name, glove_dict)
            char_embed = get_avg_ngram_embedding(col_name, 4, char_ngram_dict_list)
            column_dict[col_name] = torch.cat((word_embed, char_embed))

    # create Vocab object and load embeddings into it
    print("[generate_wiki_column_vocab] create Vocab")
    column_vocab = Vocab(emb_dict=column_dict, emb_dim=600)
    column_vocab.complete()

    return column_vocab

# given an input JSON file (WikiSQL or SENLIDB), generate Vocab for annotations (NL queries)
#field: ["sql_canonic", "nl_title"]
def generate_annot_vocab(input_json_path, glove_path, field, freq_cutoff=2):
    # determine field of JSON that contains the annotation
    print("[generate_annot_vocab] annotation field: ", field)

    with codecs.open(input_json_path, encoding='utf-8') as f:
        raw_data = json.loads(f.read())
    word_freq = defaultdict(int)
    pattern = '([\[\](){}*@&=#,.:;%$\\\'`^"_\-–²+|<>?!/\s])'
    filter_toks = {'\xa0', '', ' '}

    # retrieve Glove embeddings
    print("[generate_annot_vocab] retrieve Glove embeddings")
    glove_dict = deserialize_from_file(os.path.join(glove_path, "glove_embeddings.bin"))

    # generate dictionary with character ngrams of lengths 1-4
    char_ngram_dict_list = extract_ngram_dict(glove_dict, 4)

    # create token frequency map
    for sample in raw_data:
        query = sample[field].lower()
        query_toks = list(filter(lambda tok: tok not in filter_toks, re.split(pattern, query)))
        for token in query_toks:
            word_freq[token] += 1

    words_freq_cutoff = [w for w in word_freq if word_freq[w] >= freq_cutoff]
    ranked_words = sorted(words_freq_cutoff, key=word_freq.get, reverse=True)
    print("[generate_annot_vocab] selected words/total words = ", len(ranked_words), '/',
          len(word_freq))

    # create dictionary of word->embedding mappings
    print("[generate_annot_vocab] create query embeddings")
    word_dict = {}
    for word in ranked_words:
        word_emb = get_avg_token_embedding(word, glove_dict)
        char_emb = get_avg_ngram_embedding(word, 4, char_ngram_dict_list)
        word_dict[word] = torch.cat((word_emb, char_emb))

    # create Vocab object
    print("[generate_annot_vocab] generate Vocab object")
    vocab = Vocab(emb_dict=word_dict, emb_dim=600)
    vocab.complete()

    return vocab

#Given a textfile, create a Vocab out of its tokens
#path_to_txt_file: file with space-separated tokens to be added to the Vocabulary
#path_to_glove: path to glove embeddings file
def generate_vocab_from_file(path_to_txt_file, glove_dict, freq_cutoff = 2):
    with open(path_to_txt_file, "r") as f:
        lines = f.readlines()

    word_freq = defaultdict(int)
    pattern = '([\[\](){}*@&=#,.:;%$\\\'`^"_\-–²+|<>?!/\s])'
    filter_toks = {'\xa0', '', ' '}

    for idx, line in enumerate(lines):
        if idx % 1000 == 0:
            logging.info("Line %d" % idx)
        tokens = line.strip().split()

        for tok in tokens:
            word_freq[tok] += 1

    words_freq_cutoff = [w for w in word_freq if word_freq[w] >= freq_cutoff]
    ranked_words = sorted(words_freq_cutoff, key=word_freq.get, reverse=True)

    logging.info("selected words/total words = %d/%d" % (len(ranked_words), len(word_freq)))
    logging.info("initialize word embeddings from GloVe")
    emb_dict = {}
    for word in ranked_words:
        if word in glove_dict:
            emb_dict[word] = glove_dict[word]
        else:
            logging.debug("unknown word %s" % word)
            emb_dict[word] = glove_dict['unk']

    vocab = Vocab(emb_dict = emb_dict, emb_dim = 300)
    vocab.complete()

    return vocab


class Vocab(object):
    # emb_file: path to .bin file containing string-embedding pairs
    def __init__(self, emb_dict=None, emb_file=None, emb_dim=50):
        self.token_id_map = OrderedDict()
        self.insert_token('<pad>')
        self.insert_token('<unk>')
        self.insert_token('<eos>')
        self.emb_dict = None

        if emb_file:
            self.emb_dict = deserialize_from_file(emb_file)
        elif emb_dict:
            self.emb_dict = emb_dict

        if self.emb_dict:

            for k in self.emb_dict.keys():
                self.insert_token(k)

            self.emb_dict['<pad>'] = torch.zeros(emb_dim)
            self.emb_dict['<unk>'] = torch.rand(emb_dim)
            self.emb_dict['<eos>'] = torch.rand(emb_dim)

        else:
            self.emb_dict = {}

    @property
    def unk(self):
        return self.token_id_map['<unk>']

    @property
    def eos(self):
        return self.token_id_map['<eos>']

    def __getitem__(self, item):
        if item in self.token_id_map:
            return self.token_id_map[item]

        logging.debug('encounter one unknown word [%s]' % item)
        return self.token_id_map['<unk>']

    def __contains__(self, item):
        return item in self.token_id_map

    @property
    def size(self):
        return len(self.token_id_map)

    def __setitem__(self, key, value):
        self.token_id_map[key] = value

    def __len__(self):
        return len(self.token_id_map)

    def __iter__(self):
        return self.token_id_map.iterkeys()

    def iteritems(self):
        return self.token_id_map.iteritems()

    def complete(self):
        self.id_token_map = dict((v, k) for (k, v) in self.token_id_map.items())

    def get_token(self, token_id):
        return self.id_token_map[token_id]

    def get_id_emb(self, token_id):
        if token_id in self.id_token_map:
            tok = self.id_token_map[token_id]
            # print("[get_id_emb] ", tok)
            return self.emb_dict[tok]
        else:
            # print("[get_id_emb] unknown token_id: ", token_id)
            return self.emb_dict['<unk>']

    def insert_token(self, token):
        if token in self.token_id_map:
            return self[token]
        else:
            idx = len(self)
            self[token] = idx
            return idx
