import re
import torch
import logging
import json

class Dataset:
	#headers: dictionary with table_id -> columns info
	def __init__(self, headers, column_vocab, annot_vocab, sql_vocab):
		self.headers = headers
		self.column_vocab = column_vocab
		self.annot_vocab = annot_vocab
		self.sql_vocab = sql_vocab
		self.examples = []
		self.nl_queries_id = [] #[[tok1_ex1, tok2_ex1, ... ], [tok1_ex2, tok2_ex2, ...]]
		self.columns_id = []
		self.sql_queries_id = []
		self.labels = [] #[ground_truth_ex1, ground_truth_ex2, ...]
		self.data_matrix = {}

	def __len__(self):
		return len(self.examples)

	def load_labels(self, path_to_wiki_json):
		json_data = json.load(open(path_to_wiki_json))
		for ex in json_data:
			self.labels.append(ex["corrupted"])

	#convert train example to network-readable input
	#example_json: dictionary from wikisql_*.json corresponding to one training example
	def add_example_from_dict(self, example_json):
		table_id = example_json["table_id"]
		example_columns = [c.lower() for c in self.headers[table_id]["header"]]
		example_nl = example_json["nl_query"].lower()
		example_sql = example_json["sql_canonic"].lower()

		self.examples.append({
			"columns": example_columns,
			"nl_query": example_nl,
			"sql_canonic": example_sql
		})

		example_columns_id = [self.column_vocab[col] for col in example_columns]
		if 1 in example_columns_id:
			logging.debug("unk columns %s %s" % (example_columns, example_columns_id))

		pattern = '([\[\](){}*@&=#,.:;%$\\\'`^"_\-–²+|<>?!/\s])'
		filter_toks = {'\xa0', '', ' '}
		example_nl_toks = list(filter(lambda tok: tok not in filter_toks,
									  re.split(pattern, example_nl)))

		example_nl_id = [self.annot_vocab[tok] for tok in example_nl_toks]
		if 1 in example_nl_id:
			logging.debug("unknown nl |%s|, ids |%s|" % (example_nl_toks, example_nl_id))


		example_sql_toks = list(filter(lambda tok: tok not in filter_toks,
									   re.split(pattern, example_sql)))
		example_sql_id = [self.sql_vocab[tok] for tok in example_sql_toks]
		if 1 in example_sql_id:
			logging.debug("unknown sql |%s| ids |%s|" % (example_sql_toks, example_sql_id))

		self.labels.append(example_json["corrupted"])

		self.columns_id.append((example_columns_id))
		self.nl_queries_id.append(example_nl_id)
		self.sql_queries_id.append(example_sql_id)

	#text: corresponding string of 1 example
	def add_examples_from_text(self, src_text, tgt_text, device):
		with open(src_text, "r") as f, open(tgt_text, "r") as g:
			src_lines = f.readlines()
			tgt_lines = g.readlines()

		for (src_ex, tgt_ex) in zip(src_lines, tgt_lines):
			self.examples.append({
				"nl": src_ex.strip(),
				"sql": tgt_ex.strip()
			})
			src_toks = src_ex.strip().split()
			tgt_toks = tgt_ex.strip().split()

			src_ids = [self.annot_vocab[tok] for tok in src_toks]
			tgt_ids = [self.sql_vocab[tok] for tok in tgt_toks]

			self.nl_queries_id.append(src_ids)
			self.sql_queries_id.append(tgt_ids)

		#length statistics
		self.nl_len = [len(x) for x in self.nl_queries_id]
		self.sql_len = [len(y) for y in self.sql_queries_id]

		max_nl_len = max(self.nl_len)
		max_sql_len = max(self.sql_len)
		dataset_size = len(src_lines)

		for i in range(dataset_size):
			self.nl_queries_id[i] += (max_nl_len - self.nl_len[i]) * [0]
			self.sql_queries_id[i] += (max_sql_len - self.sql_len[i]) * [0]

		self.nl_len = torch.tensor(self.nl_len, device = device)
		self.sql_len = torch.tensor(self.sql_len, device = device)

		assert sum(len(x) for x in self.nl_queries_id) == dataset_size * max_nl_len, 'incorrect padding'
		assert sum(len(x) for x in self.sql_queries_id) == dataset_size * max_sql_len, 'incorrect padding'

		self.data_matrix["nl_queries_id"] = torch.tensor(self.nl_queries_id, device = device)
		self.data_matrix["sql_queries_id"] = torch.tensor(self.sql_queries_id, device = device)
		self.data_matrix["labels"] = torch.tensor(self.labels, device = device)

	#convert all examples to network-readable input
	#wiki_json: dictionary with dataset examples
	def add_examples(self, wiki_json, device):
		for example in wiki_json:
			self.add_example_from_dict(example)

		# length statistics
		self.column_len = [len(columns_ex) for columns_ex in self.columns_id]
		self.nl_len = [len(query_ex) for query_ex in self.nl_queries_id]
		self.sql_len = [len(sql_ex) for sql_ex in self.sql_queries_id]

		max_column_len = max(self.column_len)
		max_nl_len = max(self.nl_len)
		max_sql_len = max(self.sql_len)
		dataset_size = len(wiki_json)

		#pad lists with zeroes
		for i in range(dataset_size):
			self.columns_id[i] += (max_column_len - self.column_len[i]) * [0]
			self.nl_queries_id[i] += (max_nl_len - self.nl_len[i]) * [0]
			self.sql_queries_id[i] += (max_sql_len - self.sql_len[i]) * [0]

		self.column_len = torch.tensor(self.column_len, device = device)
		self.nl_len = torch.tensor(self.nl_len, device = device)
		self.sql_len = torch.tensor(self.sql_len, device = device)

		assert sum(len(x) for x in self.columns_id) == dataset_size * max_column_len, 'incorrect padding'
		assert sum(len(x) for x in self.nl_queries_id) == dataset_size * max_nl_len, 'incorrect padding'
		assert sum(len(x) for x in self.sql_queries_id) == dataset_size * max_sql_len, 'incorrect padding'

		self.data_matrix["columns_id"] = torch.tensor(self.columns_id, device = device)
		self.data_matrix["nl_queries_id"] = torch.tensor(self.nl_queries_id, device = device)
		self.data_matrix["sql_queries_id"] = torch.tensor(self.sql_queries_id, device = device)
		self.data_matrix["labels"] = torch.tensor(self.labels, device = device)

	#Retrieve training examples from the specified indices in the ids list
	def get_network_inputs(self, ids, device):
		assert len(ids) > 0, "No elements selected"
		return self.data_matrix["nl_queries_id"][ids].to(device),\
			   self.nl_len[ids].to(device),\
			   self.data_matrix["sql_queries_id"][ids].to(device), \
			   self.sql_len[ids].to(device),\
			   self.data_matrix["labels"][ids].to(device)

