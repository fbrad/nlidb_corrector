#!/usr/bin/env bash
gpu_id="0"
cuda_visible=4

if [ "$1" == "wikisql" ]; then
    # sql dataset
    echo "run trained model for sql"
    commandline="-batch_size 64 -max_epoch 20 -no_freeze_emb -word_embed_dim 600 -encoder_hidden_dim 1024 -gpu $gpu_id"
fi

CUDA_VISIBLE_DEVICES=${cuda_visible} python -u eval.py \
    -data "data" \
    -output_dir "output" \
    ${commandline} \

