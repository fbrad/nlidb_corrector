import torch
import logging
from torch import nn
from utils import sequence_mask
import torch.nn.functional as F

class Model(nn.Module):
	def __init__(self, args, embedding_layers):
		super().__init__()
		self.device = args.device

		self.args = args
		#self.column_emb = embedding_layers[0]
		self.annot_emb = embedding_layers[0]
		self.sql_emb = embedding_layers[1]

		#self.column_rnn = nn.LSTM(batch_first=True, input_size=args.word_embed_dim,
		#						  hidden_size=args.encoder_hidden_dim, dropout=args.dropout_rate)
		self.annot_rnn = nn.LSTM(batch_first=True, input_size=args.word_embed_dim,
								  hidden_size=args.encoder_hidden_dim, dropout=args.dropout_rate)
		self.sql_rnn = nn.LSTM(batch_first=True, input_size=args.word_embed_dim,
								  hidden_size=args.encoder_hidden_dim, dropout=args.dropout_rate)
		self.lin_layer = nn.Linear(args.encoder_hidden_dim * 3, 4)

		#self.soft = nn.Softmax(dim=1)

	def init_rnn(self, current_batch_size):
		h_0 = torch.zeros(1, current_batch_size, self.args.encoder_hidden_dim).to(self.device)
		c_0 = torch.zeros(1, current_batch_size, self.args.encoder_hidden_dim).to(self.device)
		return h_0, c_0

	#saves the Model parameters in the path specified by model_path
	def save(self, model_path="."):
		logging.info('save model to [%s]', model_path)
		torch.save(self.state_dict(), model_path)

	#loads the Model parameters saved in model_path
	def load(self, model_path):
		logging.info('load model from [%s]', model_path)
		self.load_state_dict(torch.load(model_path, map_location=self.device))# strict=False)


	#Compute an sql-aware input memory, based on the 'agreement' between the last hidden
	#state of the sql encoder (ht_sql) and every hidden state of the input memory bank
	#Returns a linear combination of the hidden states in the memory bank, using attention scores.
	#memory_bank_input: B x T x hidden_dim
	#ht_sql: B x hidden_dim
	#input_mask: B x T
	#output: B x hidden_dim
	def sql_agreement(self, memory_bank_input, ht_sql, input_mask):
		#trim end of memory bank
		relevant_max_len = input_mask.size(1)
		memory_bank_input = memory_bank_input[:,:relevant_max_len,:]
		null_mask = (1 - input_mask).type(torch.float32)

		#B x T x 1
		scores = torch.bmm(memory_bank_input, ht_sql.unsqueeze(2))

		#B x T
		att = F.softmax(scores, 1).squeeze() + (-100) * null_mask

		#mask null inputs
		#att2 = att.masked_fill_(1 - input_mask, -100.0)

		#B x 1 x T
		att = att.unsqueeze(1)
		logging.debug("attention scores %s" % att[0][0])

		#compute linear combination of memory states using attention scores
		#B x hidden_dim
		sql_aware_context = torch.bmm(att, memory_bank_input).squeeze()

		return sql_aware_context

	def old_forward(self, input):
		#B x T tensor, B tensor, ...
		column_input, column_len, annot_input, annot_len, sql_input, sql_len, labels = input

		#initialize hidden states for all RNNs
		current_batch_size = column_input.size(0)
		h0_col, c0_col = self.init_rnn(current_batch_size)
		h0_annot, c0_annot = self.init_rnn(current_batch_size)
		h0_sql, c0_sql = self.init_rnn(current_batch_size)

		#embed all inputs
		#batch x t x column_embed
		columns = self.column_emb(column_input)
		annotation = self.annot_emb(annot_input)
		sql = self.sql_emb(sql_input)

		#B x T x hidden_dim, (B X hidden_dim, B x hidden_dim)
		hidden_columns, (ht_col, ct_col) = self.column_rnn(columns, (h0_col, c0_col))
		hidden_annot, (ht_annot, ct_annot) = self.annot_rnn(annotation, (h0_annot, c0_annot))
		hidden_sql, (ht_sql, ct_sql) = self.sql_rnn(sql, (h0_sql, c0_sql))

		#retrieve last hidden state (according to input sequence length) on each
		#dimension of the batch
		#B x hidden_dim
		batch_range = torch.arange(0, current_batch_size)
		last_hidden_column = hidden_columns[batch_range, column_len - 1,:]
		last_hidden_annot = hidden_annot[batch_range, annot_len - 1, :]
		last_hidden_sql = hidden_sql[batch_range, sql_len - 1, :]
		#check correct tensors are copied
		assert last_hidden_column[2].equal(
			hidden_columns[2, column_len[2].item() - 1, :]), "unequal tensors"
		assert last_hidden_annot[1].equal(
			hidden_annot[1, annot_len[1].item() - 1, :]), "unequal tensors"
		assert last_hidden_sql[3].equal(
			hidden_sql[3, sql_len[3].item() - 1, :]), "unequal tensors"

		#B x T ByteTensor
		column_mask = sequence_mask(column_len)
		annot_mask = sequence_mask(annot_len)

		#B x hidden_dim
		col_context = self.sql_agreement(hidden_columns, last_hidden_sql, column_mask)
		annot_context = self.sql_agreement(hidden_annot, last_hidden_sql, annot_mask)

		#B x (3 * hidden_dim)
		ht_concat = torch.cat((last_hidden_column, col_context,
							   last_hidden_annot, annot_context, last_hidden_sql), dim=1)

		output = self.lin_layer(ht_concat)

		return output

	def forward(self, input):
		#B x T tensor, B tensor, ...
		annot_input, annot_len, sql_input, sql_len, labels = input

		#initialize hidden states for all RNNs
		current_batch_size = annot_input.size(0)
		h0_annot, c0_annot = self.init_rnn(current_batch_size)
		h0_sql, c0_sql = self.init_rnn(current_batch_size)

		#embed all inputs
		#batch x t x column_embed
		annotation = self.annot_emb(annot_input)
		sql = self.sql_emb(sql_input)

		#B x T x hidden_dim, (B X hidden_dim, B x hidden_dim)
		hidden_annot, (ht_annot, ct_annot) = self.annot_rnn(annotation, (h0_annot, c0_annot))
		hidden_sql, (ht_sql, ct_sql) = self.sql_rnn(sql, (h0_sql, c0_sql))

		#retrieve last hidden state (according to input sequence length) on each
		#dimension of the batch
		#B x hidden_dim
		batch_range = torch.arange(0, current_batch_size)
		last_hidden_annot = hidden_annot[batch_range, annot_len - 1, :]
		last_hidden_sql = hidden_sql[batch_range, sql_len - 1, :]

		#check correct tensors are copied
		assert last_hidden_annot[1].equal(
			hidden_annot[1, annot_len[1].item() - 1, :]), "unequal tensors"
		assert last_hidden_sql[3].equal(
			hidden_sql[3, sql_len[3].item() - 1, :]), "unequal tensors"

		#B x T ByteTensor
		annot_mask = sequence_mask(annot_len)

		#B x hidden_dim
		annot_context = self.sql_agreement(hidden_annot, last_hidden_sql, annot_mask)

		#B x (3 * hidden_dim)
		ht_concat = torch.cat((last_hidden_annot, annot_context, last_hidden_sql), dim=1)

		output = self.lin_layer(ht_concat)

		return output