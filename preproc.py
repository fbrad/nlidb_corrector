#pre-processing WikiSQL dataset
import logging
from utils import init_logging, generate_wiki_column_vocab, generate_annot_vocab, \
	generate_vocab_from_file, serialize_to_file, deserialize_from_file
import json
import copy
import os
from numpy import random
import re
import torch
from torch import nn
from Query import Query
from dataset import Dataset

#and_part: string formatted as "[ column_name ] op [ value ]"
#returns a tuple with the extracted fields ("column_name", "op", "value")
def extract_cond_fields(and_part):
	cond_reg = re.compile("\[ ([^\]]+) \] ([=<>]) \[ ([^\]]+) \]")
	res = cond_reg.search(and_part)
	if not res:
		logging.debug("can't extract fields from condition %s", and_part)
		return None
	else:
		logging.info("fields are %s", res.groups())
		return res.groups()

#returns a list of triplets ("column_name", "operator", "value")
#that represent the fields in the WHERE clause
def extract_where_fields(query):
	if "@table where" in query:
		_, where_clause = query.split("@table where")
	else:
		logging.debug("where clause not found")
		return None

	logging.info("where clause = %s", where_clause)

	and_parts = where_clause.split(" and ")
	logging.info("and parts = %s", and_parts)

	cond_fieds = [extract_cond_fields(and_part) for and_part in and_parts]

	return cond_fieds

def old_alter_aggregator(example):
	agg_dict = {0: "", 1: "max", 2: "min", 3: "count", 4: "sum", 5: "avg"}
	current_agg = example["sql"]["agg"]
	current_agg_name = agg_dict[current_agg]
	assert current_agg >= 0 and current_agg < 6, "wrong aggregator %d" % current_agg

	#remove current aggregator and randomly select from the other ones
	agg_dict.pop(current_agg)
	agg_list = list(agg_dict.keys())
	new_agg = 0#random.choice(agg_list)
	new_agg_name = agg_dict[new_agg]

	#update aggregator details
	new_example = dict(example)
	new_example["sql"]["agg"] = new_agg
	new_example["sketch"] = new_example["sketch"].lower()

	#update "sql_canonic" and "sketch" fields
	if new_agg > 0:
		print("current_agg = ", current_agg)
		re.sub(current_agg_name, new_agg_name, new_example["sql_canonic"], 1)
		re.sub(current_agg_name.upper(), new_agg_name, new_example["sketch"], 1)
	else:
		#delete aggregator ( )
		agg_regex = re.compile("(%s \( (\[[^\]]+\]) \))" % current_agg_name)
		agg_match = agg_regex.search(new_example["sql_canonic"])
		print("agg match ", agg_match)
		if not agg_match:
			logging.debug("no aggregator regex match in ", new_example["sql_canonic"])
		else:
			agg_string, no_agg_string = agg_match.groups()
			new_example["sql_canonic"] = new_example["sql_canonic"].replace(agg_string, no_agg_string)
			#TODO replace aggregator in sketch

	return new_example

#Return a new example with the modified aggregator
#example: dictionary from wikisql_*.json files corresponding to the example
#example_table_info: dictionary from headers.json corresponding to the example
def alter_aggregator(example, example_table_info):
	#alter existing aggregator
	agg_list = [0, 1, 2, 3, 4, 5]
	current_agg = example["sql"]["agg"]
	assert current_agg >= 0 and current_agg < 6, "wrong aggregator %d" % current_agg
	agg_list.remove(current_agg)
	new_agg = int(random.choice(agg_list))

	#build Query object to obtain the surface-form SQL
	query = Query(example["sql"]["sel"], new_agg, example_table_info, example["sql"]["conds"])
	new_sql = query.__repr__()

	#build new example
	new_example = copy.deepcopy(example)
	new_example["sql"]["agg"] = new_agg
	new_example["sql_canonic"] = new_sql
	new_example["sketch"] = query.generate_sketch()

	return new_example

#Return a new example with a different column selected
#example: dictionary from wikisql_*.json files corresponding to the example
#example_table_info: dictionary from headers.json corresponding to the example
def alter_column(example, example_table_info):
	all_columns = example_table_info["header"]
	header_len = len(all_columns)
	all_columns_idx = list(range(header_len))

	selected_column_idx = example["sql"]["sel"]
	all_columns_idx.remove(selected_column_idx)
	assert len(all_columns_idx) != header_len, "No column was removed"

	new_column_idx = int(random.choice(all_columns_idx))
	new_column_name = all_columns[new_column_idx]

	#build Query object
	query = Query(new_column_idx, example["sql"]["agg"], example_table_info, example["sql"]["conds"])
	new_sql = query.__repr__()

	#build new example
	new_example = copy.deepcopy(example)
	new_example["sql"]["sel"] = new_column_idx
	new_example["sql_canonic"] = new_sql
	new_example["sketch"] = query.generate_sketch()

	return new_example

#Return a new example with one of the where conditions altered. The fields that can be
#altered are either the column or the operator.
#operator statistics train: 63516 =, 6229 <, 6593 >
#example: dictionary from wikisql_*.json files corresponding to the example
#example_table_info: dictionary from headers.json corresponding to the example
def alter_where_clause(example, example_table_info):
	all_columns = example_table_info["header"]
	header_len = len(all_columns)
	all_columns_idx = list(range(header_len))
	agg_idx = [0, 1, 2, 3, 4, 5]

	where_conds = example["sql"]["conds"]
	#pick which condition is altered
	all_conds_idx = list(range(len(where_conds)))
	selected_cond_idx = int(random.choice(all_conds_idx))
	altered_cond = where_conds[selected_cond_idx]
	current_column_idx = altered_cond[0]
	current_agg_idx = altered_cond[1]

	# build new example
	new_example = copy.deepcopy(example)

	#pick if column or operator gets altered
	col_op_selector = random.choice([0, 1])
	if col_op_selector == 0:
		#alter column
		all_columns_idx.remove(current_column_idx)
		new_column_idx = int(random.choice(all_columns_idx))
		new_example["sql"]["conds"][selected_cond_idx][0] = new_column_idx
	else:
		#alter operator
		if current_agg_idx == 0:
			new_agg_idx = random.choice([1, 2])
		elif current_agg_idx == 1:
			new_agg_idx = random.choice([0, 2], p = [0.9, 0.1])
		elif current_agg_idx == 2:
			new_agg_idx = random.choice([0, 1], p = [0.9, 0.1])
		new_example["sql"]["conds"][selected_cond_idx][1] = int(new_agg_idx)

	#build Query object
	query = Query(new_example["sql"]["sel"], new_example["sql"]["agg"], example_table_info,
				  new_example["sql"]["conds"])
	new_sql = query.__repr__()
	new_example["sql_canonic"] = new_sql
	new_example["sketch"] = query.generate_sketch()

	return new_example

#dataset: json containing WikiSQL dataset
#headers: table columns info for WikiSQL
#correct_per: [0.0, 1.0] percentage of examples that remain unaltered; the other examples
#are uniformly altered; default = 0.25
#This function modifies the second half of this dataset
def alter_dataset(dataset, headers, correct_per = 0.25):
	ds_size = len(dataset)
	unaltered_size = int(correct_per * ds_size)
	altered_size = ds_size - unaltered_size
	alter_functions = [alter_aggregator, alter_column, alter_where_clause]

	#alter the second half of the dataset
	for idx, example in enumerate(dataset[unaltered_size:]):
		if idx % 100 == 0:
			logging.info("altering example %d/%d", idx, altered_size)
		table_id = example["table_id"]
		example_table_info = headers[table_id]
		#if example has no where condition, alter aggregator/column
		if example["sql"]["conds"] == []:
			alter_idx = random.choice([0, 1])
		else:
			alter_idx = random.choice([0, 1, 2])
		alter_function = alter_functions[alter_idx]
		new_ex = alter_function(example, example_table_info)
		new_ex["corrupted"] = int(alter_idx + 1)

		dataset[unaltered_size + idx] = new_ex

	#add "corrupt" = 0 label to correct example
	for idx, example in enumerate(dataset[:unaltered_size]):
		dataset[idx]["corrupted"] = 0

	return dataset

#Alter train/val/test of WikiSQL
def alter_datasets(path_to_json):
	json_names = {"wikisql_train.json": "train",
				  "wikisql_dev.json" : "dev",
				  "wikisql_test.json" : "test"}
	table_info = json.load(open(os.path.join(path_to_json, "headers.json"), 'r'))
	for json_name in json_names:
		logging.info("altering %s", json_name)
		data = json.load(open(os.path.join(path_to_json, json_name), "r"))
		data_type = json_names[json_name]

		new_data = alter_dataset(data, table_info[data_type])
		logging.info("saving altered %s", json_name)
		logging.info(90*"=")
		json.dump(new_data, open(os.path.join(path_to_json, "corrupt_" + json_name), "w"),
				  indent=2)

def create_vocabs(path_to_wiki_json):
	column_vocab = generate_wiki_column_vocab(path_to_wiki_json, path_to_wiki_json)
	annot_vocab = generate_annot_vocab(os.path.join(path_to_wiki_json, "corrupt_wikisql_train.json"),
									   path_to_wiki_json,
									   "nl_query", freq_cutoff=2)
	sql_vocab = generate_annot_vocab(os.path.join(path_to_wiki_json, "corrupt_wikisql_train.json"),
									   path_to_wiki_json,
									 "sql_canonic", freq_cutoff=2)

	serialize_to_file(column_vocab, os.path.join(path_to_wiki_json, "column_vocab.bin"))
	serialize_to_file(annot_vocab, os.path.join(path_to_wiki_json, "annot_vocab.bin"))
	serialize_to_file(sql_vocab, os.path.join(path_to_wiki_json, "sql_vocab.bin"))

	return column_vocab, annot_vocab, sql_vocab

#return an Embedding module with weights initialized from the vocab object
def create_embedding_layer(vocab, freezed = True):
	vocab_size = len(vocab)

	#create weight matrix of size num_words x emb_dim from Vocab object
	weights = torch.zeros(vocab_size, 300)

	for idx, token in enumerate(vocab.token_id_map):
		weights[idx].copy_(vocab.emb_dict[token])

	assert torch.equal(weights[23], vocab.get_id_emb(23)), "not the same embeddings"

	emb_layer = nn.Embedding.from_pretrained(weights, freeze = freezed)

	return emb_layer

#creates Datasets for training the CorrectorNet
def create_dataset_input_old(path_to_wiki_json):
	alter_datasets(path_to_wiki_json)

	train_data = json.load(open(os.path.join(path_to_wiki_json, "corrupt_wikisql_train.json"), "r"))
	dev_data = json.load(open(os.path.join(path_to_wiki_json, "corrupt_wikisql_dev.json"), "r"))
	test_data = json.load(open(os.path.join(path_to_wiki_json, "corrupt_wikisql_test.json"), "r"))
	headers = json.load(open(os.path.join(path_to_wiki_json, "headers.json"), "r"))

	column_vocab, annot_vocab, sql_vocab = create_vocabs("data")

	#create Dataset objects
	device = torch.device("cuda:0")
	for data, data_type in zip([train_data, dev_data, test_data], ["train", "dev", "test"]):
		logging.info("Creating Dataset %s" % data_type)
		dataset = Dataset(headers[data_type], column_vocab, annot_vocab, sql_vocab)
		dataset.add_examples(data, device)
		serialize_to_file(dataset, os.path.join("data", data_type + ".bin"))

#Create Dataset objects
# a) create src and tgt .txt files for train/dev/test
# b) create src and tgt Vocab
# c) create train/dev/test Dataset objects
def create_datasets(path_to_wiki_folder, corrupt_header):
	create_opennmt_dataset_input(path_to_wiki_folder, corrupt_header)

	#prepare paths
	train_wiki_path = os.path.join(path_to_wiki_folder, corrupt_header + "wikisql_train.json")
	dev_wiki_path = os.path.join(path_to_wiki_folder, corrupt_header + "wikisql_dev.json")
	test_wiki_path = os.path.join(path_to_wiki_folder, corrupt_header + "wikisql_test.json")
	train_src_path = os.path.join(path_to_wiki_folder, corrupt_header + "train_src.txt")
	train_tgt_path = os.path.join(path_to_wiki_folder, corrupt_header + "train_tgt.txt")
	dev_src_path = os.path.join(path_to_wiki_folder, corrupt_header + "dev_src.txt")
	dev_tgt_path = os.path.join(path_to_wiki_folder, corrupt_header + "dev_tgt.txt")
	test_src_path = os.path.join(path_to_wiki_folder, corrupt_header + "test_src.txt")
	test_tgt_path = os.path.join(path_to_wiki_folder, corrupt_header + "test_tgt.txt")

	glove_path = os.path.join(path_to_wiki_folder, "glove_embeddings.bin")
	glove_dict = deserialize_from_file(glove_path)
	headers = json.load(open(os.path.join(path_to_wiki_folder, "headers.json"), "r"))

	logging.info("creating vocabs")
	src_vocab = generate_vocab_from_file(train_src_path, glove_dict)
	logging.info("src vocab size = %d" % src_vocab.size)
	tgt_vocab = generate_vocab_from_file(train_tgt_path, glove_dict)
	logging.info("tgt vocab size = %d" % tgt_vocab.size)

	device = torch.device("cuda:0")
	logging.info("Creating train Dataset")
	train_dataset = Dataset(headers["train"], src_vocab, src_vocab, tgt_vocab)
	train_dataset.load_labels(train_wiki_path)
	train_dataset.add_examples_from_text(train_src_path, train_tgt_path, device)

	logging.info("Creating dev Dataset")
	dev_dataset = Dataset(headers["dev"], src_vocab, src_vocab, tgt_vocab)
	dev_dataset.load_labels(dev_wiki_path)
	dev_dataset.add_examples_from_text(dev_src_path, dev_tgt_path, device)

	logging.info("Creating test Dataset")
	test_dataset = Dataset(headers["test"], src_vocab, src_vocab, tgt_vocab)
	test_dataset.load_labels(test_wiki_path)
	test_dataset.add_examples_from_text(test_src_path, test_tgt_path, device)

	return train_dataset, dev_dataset, test_dataset


#Determine the error label for the prediction: 0 (no errors), 1 (wrong aggregator),
#2 (wrong column), 3 (wrong where condition)

#Example label 1:
#prediction: |select [ year ] from @ table where [ points ] = [ 39 ] and [ chassis ] = [ tyrrell 007 ]
#ground tr : |select sum ( [ year ] ) from @ table where [ entrant ] = [ elf team tyrrell ] and [ points ] = [ 39 ] and [ chassis ] = [ tyrrell 007 ]

#Example label 2:
#prediction: |select count ( [ events ] ) from @ table where [ tournament ] = [ pga championship and evens ] and [ events ] < [ 4 ]
#ground tr : |select count ( [ top - 5 ] ) from @ table where [ tournament ] = [ pga championship ] and [ events ] < [ 4 ]

def compute_error_label(prediction, ground_truth):
	logging.info("pred |%s|" % prediction.strip())
	logging.info("gt   |%s|" % ground_truth.strip())
	if prediction == ground_truth:
		logging.info("	no error")
		return 0

	pred_toks = prediction.strip().split()
	ground_toks = ground_truth.strip().split()

	#check aggregator
	if pred_toks[1] != ground_toks[1]:
		logging.info("	aggregator error")
		return 1

	#check column
	from_idx = ground_toks.index("from")
	if pred_toks[:from_idx] != ground_toks[:from_idx]:
		logging.info("	column error")
		return 2

	logging.info("	where error")
	return 3


#creates Dataset based on the SQLs decoded from SEQ2SEQ
#test_src_path: path to WikiSQL test source text (columns and annotations)
#test_tgt_path: path to WikiSQL test target text (sql)
#decoded_txt: path to file where SEQ2SEQ predictions on WikiSQL test are stored
#Returns a Dataset readily-consumed by the NetPredictor
def create_decoded_test_dataset(test_src_path, test_tgt_path, decoded_path, path_to_wiki_json):
	with open(test_src_path) as f, open(test_tgt_path) as g, open(decoded_path, "r") as h:
		wiki_test_src = f.readlines()
		wiki_test_tgt = g.readlines()
		predictions = h.readlines()

	corrupt_train_dataset = deserialize_from_file(os.path.join(path_to_wiki_json, "train.bin"))
	annot_vocab, sql_vocab = corrupt_train_dataset.annot_vocab, corrupt_train_dataset.sql_vocab
	headers = json.load(open(os.path.join(path_to_wiki_json, "headers.json"), "r"))
	#wikisql_test_data = json.load(open(os.path.join(path_to_wiki_json, "wikisql_test.json"), "r"))

	#replace original sql with decoded one
	#update labels
	acc = 0.0
	labels = []
	for (y, pred) in zip(wiki_test_tgt, predictions):
		label = compute_error_label(pred, y)
		labels.append(label)
		if label == 0:
			acc += 1

	logging.info("WikiSQL test accuracy %f" % (acc / len(wiki_test_src)))
	device = torch.device("cuda:0")

	#create new Dataset
	decoded_test_dataset = Dataset(headers["test"], annot_vocab, annot_vocab, sql_vocab)
	decoded_test_dataset.labels = labels
	decoded_test_dataset.add_examples_from_text(test_src_path, decoded_path, device)

	return decoded_test_dataset

#creates source and target .txt files to be fed to OpenNMT system/CorrectorNet
#examples are taken from wikisql_*.json; tokenization is similar to the one used
#for the corrector network
#corrupt_header: prefix that separates clean queries JSON from corrupted queries JSON.
def create_opennmt_dataset_input(path_to_wiki_json, corrupt_header = "corrupt_"):
	headers = json.load(open(os.path.join(path_to_wiki_json, "headers.json"), "r"))
	json_names = {corrupt_header + "wikisql_train.json": "train",
				  corrupt_header + "wikisql_dev.json" : "dev",
				  corrupt_header + "wikisql_test.json" : "test"}
	pattern = '([\[\](){}*@&=#,.:;%$\\\'`^"_\-–²+|<>?!/\s])'
	filter_toks = {'\xa0', '', ' '}

	table_map = {}
	for json_name in json_names:
		data_type = json_names[json_name]
		data = json.load(open(os.path.join(path_to_wiki_json, json_name), "r"))
		data_size = len(data)
		src_txt = open(os.path.join(path_to_wiki_json, corrupt_header + data_type + "_src.txt"), "w")
		tgt_txt = open(os.path.join(path_to_wiki_json, corrupt_header + data_type + "_tgt.txt"), "w")

		for idx, example in enumerate(data):
			if idx % 1000 == 0:
				logging.info("preprocess example %d/%d" % (idx, data_size))
			table_id = example["table_id"]
			table_columns = [col.lower() for col in headers[data_type][table_id]["header"]]
			nl_text = example["nl_query"].lower()
			nl_toks = list(filter(lambda tok: tok not in filter_toks, re.split(pattern, nl_text)))
			sql_text = example["sql_canonic"].lower()
			sql_toks = list(filter(lambda tok: tok not in filter_toks, re.split(pattern, sql_text)))
			target_text = " ".join(sql_toks)

			#tokenize columns and separate them with a special token <end>
			source_tokens = [] #[columns; nl description]
			for column in table_columns:
				#tokenize column
				column_toks = list(filter(lambda tok: tok not in filter_toks, re.split(pattern, column)))
				source_tokens += column_toks
				source_tokens.append("<end>")

			source_tokens += nl_toks
			source_text = " ".join(source_tokens)

			src_txt.write(source_text + "\n")
			tgt_txt.write(target_text + "\n")

	src_txt.close()
	tgt_txt.close()


if __name__ == '__main__':
	init_logging('preproc.log', logging.INFO)

	#train, dev, test = create_datasets("data", "corrupt_")
	#serialize_to_file(train, os.path.join("data", "train.bin"))
	#serialize_to_file(dev, os.path.join("data", "dev.bin"))
	#serialize_to_file(test, os.path.join("data", "test.bin"))

	#create_opennmt_dataset_input("data", "")
	test_decoded = create_decoded_test_dataset("data/test_src.txt", "data/test_tgt.txt",
								"data/pred_step_10000.txt", "data")
	serialize_to_file(test_decoded, "data/test_decoded.bin")
	#test = create_test_dataset("data", "output/pred_step_10000.txt")


